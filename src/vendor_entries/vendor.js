// All files in this file will be included into vendor.js file.

module.exports = [
  './node_modules/jquery/dist/jquery.min.js',
  './node_modules/@editorjs/editorjs/dist/editor.js',
  './node_modules/@editorjs/embed/dist/bundle.js',
  './node_modules/@editorjs/list/dist/bundle.js',
  './node_modules/@editorjs/header/dist/bundle.js',
  './node_modules/@editorjs/raw/dist/bundle.js',
];
