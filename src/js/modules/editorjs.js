const editorjs = (($) => {
  function init() {
    const editor = new EditorJS({
      holderId: 'editorjs',
      tools: {
        header: {
          class: Header,
          inlineToolbar: ['link'],
        },
        list: {
          class: List,
          inlineToolbar: [
            'link',
            'bold',
          ],
        },
        embed: {
          class: Embed,
          inlineToolbar: false,
          config: {
            youtube: true,
            coub: true,
          },
        },
        raw: RawTool,
      },
    });

    const saveBtn = document.querySelector('button');

    saveBtn.addEventListener('click', () => {
      editor.save().then((outputData) => {
        console.log('Article data: ', outputData); // store to database
      }).catch((error) => {
        console.log('Saving failed: ', error);
      });
    });
  }

  return { init };
})(jQuery);

export default editorjs;
