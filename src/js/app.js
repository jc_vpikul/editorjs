// You can write a call and import your functions in this file.
//
// This file will be compiled into app.js
// Feel free with using ES6 here.

import editorjs from './modules/editorjs';

(($) => {
  // When DOM is ready
  $(() => {
    editorjs.init();
  });
})(jQuery);
